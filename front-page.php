<?php while (have_posts()) : the_post(); ?>

	<div class="slider-container">
		<section class="index-slider">
			<?php if( have_rows('intro_slider') ):?>
				<?php while ( have_rows('intro_slider') ) : ?>
					<?php the_row(); ?>
					<?php $image=get_sub_field('intro_slider_image');?>
					<section class="content-block"  style="background-image: url(<?php echo $image['sizes']['large']; ?>)">
						<!-- <img class="slick-img" src="<?php echo $image['sizes']['large']; ?>" alt=""> -->
						<div class="slide-info">
							<h2 class="slide-title"><?php the_sub_field('intro_slider_title');?></h2>
							<p class="slide-description">
								<?php the_sub_field('intro_slider_subtitle');?>
							</p>

							<?php  if( have_rows('intro_slider_buttons') ): ?>
								<?php while( have_rows('intro_slider_buttons') ): the_row(); ?>
									<?php if (get_sub_field('intro_slider_buttons_link')): ?>
										<a href="<?php the_sub_field('intro_slider_buttons_link'); ?>" class="btn product-btn"> 
											<?php the_sub_field('intro_slider_buttons_name'); ?> 
										</a>
									<?php else : ?>
										<a class="btn product-btn" data-open="call">
											<?php the_sub_field('intro_slider_buttons_name'); ?>
										</a>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
						<img class="arrow_bottom slider_bottom" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_bottom.svg" alt="">
					</section>

				<?php  endwhile; ?>
			<?php endif; ?>
		</section>
	</div>

	<div class="call-mobile-btn btn product-btn" data-open="call">ЗВ’ЯЗАТИСЬ З НАМИ</div>


	<section class="our_products" id="our_products">
		<h3 class="simple-title"><?php the_field('catalog_title');?></h3>
		<ul class="tabs" data-tabs id="products">
			<?php 
			$terms = get_terms( 'products_cat' );
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
				$first_element=true;
				foreach ( $terms as $term ) {
					if ($first_element) 
						echo '<li class="tabs-title is-active"><a href="#' . $term->slug . '" aria-selected="true">' . $term->name . '</a></li>';
					else
						echo '<li class="tabs-title"><a href="#' . $term->slug . '">' . $term->name . '</a></li>';
					$first_element=false;
				}
			}
			?>
		</ul>
		<div class="tabs-content" data-tabs-content="products">
			<?php  $terms = get_terms( 'products_cat' );?>
			<?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :  $first_element_class='is-active'; ?>
				<?php  foreach ( $terms as $term ) : ?>

					<div class="tabs-panel <?= $first_element_class;?>" id="<?= $term->slug;?>">
						<div class="row">
							<?php $first_element_class='';
							$args = array(
								'post_type'              => array( 'products' ),
								'tax_query'              => array(
									'relation' => 'AND',
									array(
										'taxonomy'         => 'products_cat',
										'terms'            => $term->term_id,
										),
									),
								);
								$query = new WP_Query( $args ); ?>
								<?php if ( $query->have_posts() ) :  ?>
									<?php while ( $query->have_posts() ) : $query->the_post();  ?>
										<?php get_template_part( 'templates/block', 'loop_one_product' );?>
									<?php endwhile;  ?>
									<?php wp_reset_postdata();?>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif;  ?>
			</div>
		</div>
	</section>


	<section class="how-to-order">
		<h3 class="simple-title"><?php the_field('schema_title');?></h3>
		<div class="order-steps">
			<div class="row">
				<?php if( have_rows('schema') ):?>
					<?php while ( have_rows('schema') ) : ?>
						<?php the_row(); ?>

						<div class="columns medium-3 step">
							<div class="step-round">
								<img src="<?php the_sub_field('step_icon');?>" alt="">
							</div>
							<h4><?php the_sub_field('step_name');?></h4>
							<p><?php the_sub_field('step_list');?></p>
						</div>
					<?php  endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>


	<section class="positives">
		<div class="row">
			<?php if( have_rows('statistics') ):?>
				<?php while ( have_rows('statistics') ) : ?>
					<?php the_row(); ?>
					<div class="columns medium-4">
						<div class="pos-item">
							<div class="count"><?php the_sub_field('statistic_metrics');?></div>
							<p><?php the_sub_field('statistic_metric_title');?></p>
						</div>
					</div>
				<?php  endwhile; ?>
			<?php endif; ?>
		</div>
	</section>


	<section class="about-us">
		<div class="back-colored">
			<h3 class="simple-title white-title"><?php the_field('about_title');?></h3>
			<div class="row">
				<div class="columns medium-4">
					<?php the_field('about_text1');?>
				</div>
				<div class="columns medium-4">
					<?php the_field('about_text2');?>
				</div>
				<div class="columns medium-4">
					<?php the_field('about_text3');?>
				</div>
			</div>
			<a href="<?php the_field('about_button_link');?>" class="btn btn-white"><?php the_field('about_button_text');?></a>
		</div>
	</section>

	<section class="advantages">
		<div class="row page-container">
			<?php if( have_rows('advantages') ):?>
				<?php while ( have_rows('advantages') ) : ?>
					<?php the_row(); ?>
					<div class="columns medium-3 step">
						<div class="step-round">
							<img src="<?php the_sub_field('advantages_icon');?>" alt="">
						</div>
						<h4><?php the_sub_field('advantages_name');?></h4>
						<!-- <p><?php the_sub_field('advantages_text');?></p> -->
					</div>
				<?php  endwhile; ?>
			<?php endif; ?>
		</div>
	</section>

	<section class="our-projects" id="objects">
		<h3 class="simple-title white-title">Наші проекти</h3>
		<div class="row">
			<div class="columns medium-3 example-item" data-open="example" id="example_1">
				<div class="img-wrap">
					<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
					<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
				</div>
				<div class="example-descr">
					<h5>Проект № 6 <br>
						Бруківка "Старе місто"</h5>
						<p>Місце знаходження: м.Івано-Франківськ
							Проект було виконано на протязі 14 днів.
							Декоративні блоки: Блок “Рядовий”.
							Тип бруківки:  “ Цегла рядова”.
							Було підібрано потрібні кольори...</p>
						</div>
					</div>
					<div class="columns medium-3 example-item" data-open="example"id="example_2">
						<div class="img-wrap">
							<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
							<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
						</div>
						<div class="example-descr">
							<h5>Проект № 6 <br>
								Бруківка "Старе місто"</h5>
								<p>Місце знаходження: м.Івано-Франківськ
									Проект було виконано на протязі 14 днів.
									Декоративні блоки: Блок “Рядовий”.
									Тип бруківки:  “ Цегла рядова”.
									Було підібрано потрібні кольори...</p>
								</div>
							</div>
							<div class="columns medium-3 example-item" data-open="example" id="example_3">
								<div class="img-wrap">
									<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
									<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
								</div>
								<div class="example-descr">
									<h5>Проект № 6 <br>
										Бруківка "Старе місто"</h5>
										<p>Місце знаходження: м.Івано-Франківськ
											Проект було виконано на протязі 14 днів.
											Декоративні блоки: Блок “Рядовий”.
											Тип бруківки:  “ Цегла рядова”.
											Було підібрано потрібні кольори...</p>
										</div>
									</div>
									<div class="columns medium-3 example-item" data-open="example" id="example_4">
										<div class="img-wrap">
											<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
											<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
										</div>
										<div class="example-descr">
											<h5>Проект № 6 <br>
												Бруківка "Старе місто"</h5>
												<p>Місце знаходження: м.Івано-Франківськ
													Проект було виконано на протязі 14 днів.
													Декоративні блоки: Блок “Рядовий”.
													Тип бруківки:  “ Цегла рядова”.
													Було підібрано потрібні кольори...</p>
												</div>
											</div>
											<div class="columns medium-3 example-item" data-open="example" id="example_5">
												<div class="img-wrap">
													<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
													<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
												</div>
												<div class="example-descr">
													<h5>Проект № 6 <br>
														Бруківка "Старе місто"</h5>
														<p>Місце знаходження: м.Івано-Франківськ
															Проект було виконано на протязі 14 днів.
															Декоративні блоки: Блок “Рядовий”.
															Тип бруківки:  “ Цегла рядова”.
															Було підібрано потрібні кольори...</p>
														</div>
													</div>
													<div class="columns medium-3 example-item" data-open="example" id="example_6">
														<div class="img-wrap">
															<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
															<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
														</div>
														<div class="example-descr">
															<h5>Проект № 6 <br>
																Бруківка "Старе місто"</h5>
																<p>Місце знаходження: м.Івано-Франківськ
																	Проект було виконано на протязі 14 днів.
																	Декоративні блоки: Блок “Рядовий”.
																	Тип бруківки:  “ Цегла рядова”.
																	Було підібрано потрібні кольори...</p>
																</div>
															</div>
															<div class="columns medium-3 example-item" data-open="example" id="example_7">
																<div class="img-wrap">
																	<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
																	<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
																</div>
																<div class="example-descr">
																	<h5>Проект № 6 <br>
																		Бруківка "Старе місто"</h5>
																		<p>Місце знаходження: м.Івано-Франківськ
																			Проект було виконано на протязі 14 днів.
																			Декоративні блоки: Блок “Рядовий”.
																			Тип бруківки:  “ Цегла рядова”.
																			Було підібрано потрібні кольори...</p>
																		</div>
																	</div>
																	<div class="columns medium-3 example-item" id="example_8" data-open="example">
																		<div class="img-wrap">
																			<img src="<?php echo get_template_directory_uri();?>/dist/images/example1.png" alt="">
																			<img src="<?php echo get_template_directory_uri();?>/dist/images/example2.png" alt="">
																		</div>
																		<div class="example-descr">
																			<h5>Проект № 6 <br>
																				Бруківка "Старе місто"</h5>
																				<p>Місце знаходження: м.Івано-Франківськ
																					Проект було виконано на протязі 14 днів.
																					Декоративні блоки: Блок “Рядовий”.
																					Тип бруківки:  “ Цегла рядова”.
																					Було підібрано потрібні кольори...</p>
																				</div>
																			</div>
																		</div>
																	</section>

																	

																	<div class="reveal" id="call" data-reveal>
																		<div class="modal_content">
																			<h5>Залиште свій телефон<br>
																				і ми вам перетелефонуємо</h5>
																				<form action="" class="feedback-modal">
																					<div class="form-group">
																						<label for="name">Ім'я <span>*</span></label>
																						<input type="text" id="name" name="name">
																					</div>
																					<div class="form-group has-error">
																						<label for="phone">Номер телефону <span>*</span></label>
																						<input type="text" id="phone" name="phone">
																					</div>
																					<!--<div class="form-group">-->
																					<a href="/index.html" class="btn product-btn">Відправити</a>
																					<!--</div>-->
																					<div class="clearfix"></div>
																				</form>

																				<button class="close-button" data-close aria-label="Close modal" type="button">
																					<span aria-hidden="true"><img src="<?php echo get_template_directory_uri();?>/dist/images/close.svg" alt="close"></span>
																				</button>
																			</div>
																		</div>

																		<!--<div class="" id="example" >-->
																		<div class="reveal" id="example" data-reveal>
																			<div class="gallery_modal_content">
																				<section class="gallery-slider">
																					<section class="content-block-gallery">
																						<img src="<?php echo get_template_directory_uri();?>/dist/images/slide1.png" alt="">
																						<div class="example-descr">
																							<h5>Проект № 6 <br>
																								Бруківка "Старе місто"</h5>
																								<p>Місце знаходження: м.Івано-Франківськ
																									Проект було виконано на протязі 14 днів.
																									Декоративні блоки: Блок “Рядовий”.
																									Тип бруківки:  “ Цегла рядова”.
																									Було підібрано потрібні кольори...</p>
																								</div>
																							</section>
																							<section class="content-block-gallery">
																								<img src="<?php echo get_template_directory_uri();?>/dist/images/slide1.png" alt="">
																								<div class="example-descr">
																									<h5>Проект № 6 <br>
																										Бруківка "Старе місто"</h5>
																										<p>Місце знаходження: м.Івано-Франківськ
																											Проект було виконано на протязі 14 днів.
																											Декоративні блоки: Блок “Рядовий”.
																											Тип бруківки:  “ Цегла рядова”.
																											Було підібрано потрібні кольори...</p>
																										</div>
																									</section>
																									<section class="content-block-gallery">
																										<img src="<?php echo get_template_directory_uri();?>/dist/images/slide1.png" alt="">
																										<div class="example-descr">
																											<h5>Проект № 6 <br>
																												Бруківка "Старе місто"</h5>
																												<p>Місце знаходження: м.Івано-Франківськ
																													Проект було виконано на протязі 14 днів.
																													Декоративні блоки: Блок “Рядовий”.
																													Тип бруківки:  “ Цегла рядова”.
																													Було підібрано потрібні кольори...</p>
																												</div>
																											</section>
																										</section>

																										<button class="close-button" data-close aria-label="Close modal" type="button">
																											<span aria-hidden="true"><img src="<?php echo get_template_directory_uri();?>/dist/images/close.svg" alt="close"></span>
																										</button>
																									</div>
																								</div>
																							<?php endwhile; ?>


																							<script type="text/javascript">
																								jQuery(document).ready(function() {
																									jQuery('.index-slider').slick({
																										prevArrow: '<img class="arrow arrow_left" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_left.svg" alt="">',
																										nextArrow: '<img class="arrow arrow_right" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_right.svg" alt="">',
																										infinite: true,
																										dots: true,
																										speed: 1000,
																										autoplay: true,
																										autoplaySpeed: 10000,
																									});

																									var slide_img_height;
																									jQuery('.content-block').each(function() {
																										var url  = jQuery('.content-block').css('background-image').replace('url(', '').replace(')', '').replace("'", '').replace('"', '').replace('"', '');
																										var bgImg = jQuery('<img />');
																										bgImg.hide();
																										jQuery(this).append(bgImg);
																										bgImg.attr('src', url);
																										bgImg.bind('load', function()
																										{
																											slide_img_height = bgImg.height();
																											//                    array_height.push(slide_img_height);
																											console.log(slide_img_height);
																											jQuery('.index-slider .slick-slide').css('min-height', slide_img_height);
																											jQuery('.content-block').css('min-height', slide_img_height);
																										});
																									});


																									var modal_slide_height =  jQuery(window).height();
																									jQuery('.gallery_modal_content').css('height', modal_slide_height);

																									var contact_height =  jQuery('.contacts').height();
																									jQuery('.contact').css('height', contact_height);

																									jQuery(".slider_bottom").click(function() {
																										jQuery('html, body').animate({
																											scrollTop: jQuery("#our_products").offset().top
																										}, 1000);
																									});
																									jQuery('.gallery-slider').slick({
																										prevArrow: '<img class="arrow arrow_left" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_left.svg" alt="">',
																										nextArrow: '<img class="arrow arrow_right" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_right.svg" alt="">',
																										infinite: true,
																										dots: false,
																										speed: 1000,
																										autoplay: false
																									});
																									jQuery(".example-item").click(function(e) {
																										e.preventDefault();
																										var example_id = jQuery(this).attr('id');
																										setTimeout(function() {
																											jQuery('.gallery-slider').slick("setPosition", 0);
																										}, 10)
																									});
																									jQuery('.close-menu').click(function() {
																										jQuery('#menu').css('display', 'none');
																									})
																								});
																							</script>