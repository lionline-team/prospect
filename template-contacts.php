<?php
/**
 * Template Name: Contacts Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

	<ul  class="breadcrumbs">
		<ul  class="breadcrumbs">
			<?php yoast_breadcrumb('<li>','</li>');?>
		</ul>
	</ul>
	<div class="contact-container">
		<h4 class="cat-title">КОНТАКТИ КОМПАНІЯ ПРОСПЕКТ - ВИГОТОВЛЕННЯ БРУКІВКИ</h4>
	</div>

	<section class="contacts-detail">
		<div class="row contact-container">
			<div class="columns">
				<h5>76011, Івано-Франківська область<br>
					місто Івано-Франківськ<br>
					вулиця Євгена Коновальця, 227-А</h5>
				</div>
				<div class="columns">
					<h5>Графік роботи:</h5>
					<h5>Пн-Пт: 0900 — 1700</h5>
					<h5>Сб. Нд. - Вихідний</h5>
					<h5>обід з 13:00 до 1400</h5>
				</div>
				<div class="columns">
					<h5 class="prod-name">Зв'яжіться з нами за телефоном:</h5>
					<h5 class="prod-name">+38 099 74 12 829</h5>
					<h5 class="prod-name">+38 098 72 91 703</h5>
				</div>
				<div class="columns other-cont">
					<h5>Слідкуйте за нами в
						соціальних мережах</h5>
						<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/instagram.svg" alt=""></a>
						<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/facebook.svg" alt=""></a>
						<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/twit.svg" alt=""></a>
					</div>
				</div>
			</section>

			<section class="maps">
				<div class="row contact-container">
					<h4 class="color-title">Карта проїзду</h4>
                    <iframe id="map" src="https://snazzymaps.com/embed/110515" width="100%" height="542px" style="border:none;"></iframe>
				</div>
			</section>

		<?php endwhile; ?>

		<script type="text/javascript">
			jQuery(document).ready(function() {
				var contact_height =  jQuery('.contacts').height();
				jQuery('.contact').css('height', contact_height);

				var container =  jQuery('.about_blocks .page-container').width();
				var img_width =  jQuery('.about_blocks img').width();
				var img_height =  jQuery('.about_blocks img').height();
				jQuery('.block-text').css({'width': container - img_width, 'height': img_height});

				jQuery('.close-menu').click(function() {
					jQuery('#menu').css('display', 'none');
				})
			});
		</script>