<?php
/**
 * Template Name: AboutUs Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

	<ul  class="breadcrumbs">
		<?php yoast_breadcrumb( '<li>','</li>' ); ?>
	</ul>
	<div class="page-container">
		<h4 class="cat-title">ВИГОТОВЛЕННЯ БРУКІВКИ - КОМПАНІЯ PROSPECT</h4>
	</div>

	<section class="about_blocks">
		<div class="row page-container">
			<img src="<?php echo get_template_directory_uri();?>/dist/images/about_block1.png" alt="">
			<div class="columns block-text">
				<h5 class="about-title">ОСНОВНИМ НАПРЯМКОМ ДІЯЛЬНОСТІ Є ВИГОТОВЛЕННЯ ТА
					РЕАЛІЗАЦІЯ БРУКІВКИ ТА  БЕТОННИХ ВИРОБІВ.</h5>
					<p>Ми, збудований у 2018 р. інноваційний завод по виготовленню бруківки.
						Завдяки автоматизованому виробництву та  обладнанню ми можемо гарантувати
						високу якість продукції та своєчасне виконання замовлень. У своїй роботі
						ми застосовуємо сучасне обладнання та високоякісні матеріали. Прогресивні
						методи виробництва та висока кваліфікація наших працівників дозволяють
						добиватися найвищих експлуатаційних характеристик.</p>
					</div>
				</div>
				<div class="call-mobile-btn btn product-btn" data-open="call" aria-controls="call" aria-haspopup="true" tabindex="0">ЗВ’ЯЗАТИСЬ З НАМИ</div>
			</section>


			<section class="positives">
				<div class="row">
					<div class="columns medium-4">
						<div class="pos-item">
							<div class="count">685</div>
							<p>вчасно виконаних замовлень за останній рік</p>
						</div>
					</div>
					<div class="columns medium-4">
						<div class="pos-item">
							<div class="count">28</div>
							<p>хвилин нам потрібно, щоб виробити 1 м2 першокласної бруківки</p>
						</div>
					</div>
					<div class="columns medium-4">
						<div class="pos-item">
							<div class="count">452</div>
							<p>позитивних відгуків про нашу продукцію від замовників</p>
						</div>
					</div>
				</div>
			</section>

			<section class="about_blocks">
				<div class="row page-container">
					<div class="columns block-text">
						<h5 class="about-title">ЧОМУ ВАРТО ОБРАТИ НАС?</h5>
						<p>Перевага нашої продукції в тому, що  виготовляється за прогресивною технологією
							з додаванням спеціальних сумішей і присадок,  підвищують довговічність,
							надійність та інші характеристики виробів. Бетонні вироби виготовлені за
							даною технологією мають строгу геометрію, що полегшує роботу при будівництві
							чи ремонті об’єктів. </p>
						</div>
						<img src="<?php echo get_template_directory_uri();?>/dist/images/about_block2.png" alt="">
					</div>
				</section>

				<section class="advantages">
					<div class="row page-container">
						<div class="columns medium-3 step">
							<div class="step-round">
								<img src="<?php echo get_template_directory_uri();?>/dist/images/adv_step1.svg" alt="">
							</div>
							<h4>Нове обладнання</h4>
						</div>
						<div class="columns medium-3 step">
							<div class="step-round">
								<img src="<?php echo get_template_directory_uri();?>/dist/images/adv_step2.svg" alt="">
							</div>
							<h4>Висока якість</h4>
						</div>
						<div class="columns medium-3 step">
							<div class="step-round">
								<img src="<?php echo get_template_directory_uri();?>/dist/images/adv_step3.svg" alt="">
							</div>
							<h4>Комп'ютеризоване виробництво</h4>
						</div>
						<div class="columns medium-3 step">
							<div class="step-round">
								<img src="<?php echo get_template_directory_uri();?>/dist/images/adv_step4.svg" alt="">
							</div>
							<h4>Хороша ціна</h4>
						</div>
					</div>
				</section>

			<?php endwhile; ?>
