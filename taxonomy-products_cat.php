<ul  class="breadcrumbs">
	<?php yoast_breadcrumb('<li>','</li>');?>
</ul>

<section class="catalogue-descr">
	<div class="back-colored">
		<h4 class="cat-title"><?php single_cat_title(); ?></h4>
		<p>
			<?php echo category_description( ); ?> 
			<?php  $terms = get_terms( 'products_cat' ); ?>
			<?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) : ?>
				<div class="btn-wrap">
					<?php 
					foreach ( $terms as $term ) 
						echo '<a href="' . esc_url( get_term_link( $term ) ) . '" class="btn product-btn">' . $term->name . '</a>';
					?>
					<div class="clearfix"></div>
				</div>
			<?php endif; ?>
		</p>

	</div>
</section>

<section class="our_products catalogue">
	<div class="row">

		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'templates/block', 'loop_one_product' );?>
		<?php endwhile; ?>

	</div>
</section>