<ul  class="breadcrumbs">
	<?php yoast_breadcrumb('<li>','</li>');?>
</ul>

<section class="catalogue-descr">
	<div class="back-colored">
		<h4 class="cat-title"><?= post_type_archive_title(); ?></h4>
		<p>
			Сьогодні застосовують дві основні технології виготовлення бруківки — вібропресування та вібролиття. Завдяки більш точній відповідності сучасним вимогам до даного виду продукції метод вібропресування має значні конкурентні переваги, а тому практично витіснив вібролиття.
		</p>
		<?php $terms = get_terms( 'products_cat' ); ?>
		<?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) : ?>
			<div class="btn-wrap">
				<?php 
				foreach ( $terms as $term ) 
					echo '<a href="' . esc_url( get_term_link( $term ) ) . '" class="btn product-btn">' . $term->name . '</a>';
				?>
				<div class="clearfix"></div>
			</div>
		<?php endif; ?>
		
	</div>
</section>

<section class="our_products catalogue">
	<div class="row">

		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'templates/block', 'loop_one_product' );?>
		<?php endwhile; ?>

	</div>
</section>