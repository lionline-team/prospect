<div class="columns medium-4 cur-item">
	<a href="<?php the_permalink();?>"><div class="item-wrap-img"><img src="<?php the_field('thumbnail');?>" alt="pavement"></div></a>
	<div class="prod-info">
		<h4 class="prod-name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
		<section class="prod-description">
			<div class="prod-item">
				<div class="item-left">Розміри, см</div>
				<div class="item-right"><?php the_field('dimentions');?></div>
				<div class="clearfix"></div>
			</div>
			<div class="prod-item">
				<div class="item-left">Колір</div>
				<div class="item-right">
					<?php while ( have_rows('colours') ) : ?>
						<?php the_row(); ?>
						<span class="color-filter white" style="background-color: <?php the_sub_field('colour');?>;"></span>
					<?php  endwhile; ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="prod-item">
				<div class="item-left">Ціна</div>
				<div class="item-right"><?php the_field('price');?></div>
				<div class="clearfix"></div>
			</div>
		</section>
		<a href="<?php the_permalink();?>" class="order-btn">Замовити</a>
		<div class="clearfix"></div>
	</div>
</div>