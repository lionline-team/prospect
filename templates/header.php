  <header>
    <nav class="navigation">
      <div class="title-logo left"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.svg"></a></div>
      <div class="title-bar" data-responsive-toggle="menu" data-hide-for="large">
        <button class="menu-icon" type="button" data-toggle></button>
      </div>
      <div class="menu-wrap">
        <ul class="dropdown menu" data-disable-hover="true" id="menu" data-click-open="true" data-dropdown-menu>
         <?php if (has_nav_menu('primary_navigation')) :?>
          <?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
        <?php endif;?>

        <button class="close-button close-menu" type="button">
          <span aria-hidden="true"><img src="<?php echo get_template_directory_uri();?>/dist/images/close.svg" alt="close"></span>
        </button>
      </ul>
      <div class="btn-group right">
        <!-- <span class="lang_switcher"><a href="">UA</a></span> -->
        <div class="call-btn" data-open="call">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/call.svg">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/call_hover.svg">
        </div>
        <div class="search">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/search.svg" alt="">
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
  </nav>
</header>
<div class="clearfix"></div>
