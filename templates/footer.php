<section class="contacts">
	<div class="row">
		<div class="columns medium-6 contact">
			<h4 class="color-title">Контакти</h4>
			<div class="address contact-info">
				<h4 class="prod-name">Адреса</h4>
				<p>76011, Івано-Франківська область<br>
					місто Івано-Франківськ<br>
					вулиця Євгена Коновальця, 227-А
				</p>
				<section class="other-cont">
					<div class="prod-name">Зв'яжіться з нами за телефоном:</div>
					<h4 class="prod-name">+38 099 74 12 829</h4>
					<h4 class="prod-name">+38 098 72 91 703</h4>
				</section>
			</div>
			<div class="schedule contact-info">
				<h4 class="prod-name">Графік роботи</h4>
				<p>Пн-Пт: 0900 — 1700</p>
				<p>Сб. Нд. - Вихідний</p>
				<p>обід з 13:00 до 1400</p>
				<section class="other-cont">
					<div class="prod-name">Слідкуйте за нами в
						соціальних мережах
					</div>
					<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/instagram.svg" alt=""></a>
					<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/facebook.svg" alt=""></a>
					<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/twit.svg" alt=""></a>
				</section>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="columns medium-6 feedback">
			<h4 class="color-title">Зворотній зв'язок</h4>
			<form action="" class="feedback-form">
				<div class="form-group">
					<label for="name">Ім'я <span>*</span></label>
					<input type="text" id="name" name="name">
				</div>
				<div class="form-group has-error">
					<label for="phone">Номер телефону <span>*</span></label>
					<input type="text" id="phone" name="phone">
				</div>
				<div class="form-group textarea">
					<label for="text">Текст вашого повідомлення <span>*</span></label>
					<textarea type="text" id="text" name="text"></textarea>
				</div>
				<a href="/index.html" class="btn product-btn item-right">Зв'язатися з нами</a>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
</section>

<footer>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.svg" alt=""></a>
	<div class="copyright">Created by LionLine © 2018</div>
</footer>