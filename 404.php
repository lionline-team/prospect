<ul  class="breadcrumbs">
    <?php yoast_breadcrumb('<li>','</li>');?>
</ul>

<div class="missing_wrap">
    <section class="not_found">
        <div class="pavement_animation">
            <span>4</span>
            <img src="<?php echo get_template_directory_uri();?>/dist/images/first.svg" class="first_elem" alt="">
            <img src="<?php echo get_template_directory_uri();?>/dist/images/second.svg" class="second_elem" alt="">
            <img src="<?php echo get_template_directory_uri();?>/dist/images/fourth.svg" class="fourth_elem" alt="">
            <img src="<?php echo get_template_directory_uri();?>/dist/images/third.svg" class="third_elem" alt="">
            <img src="<?php echo get_template_directory_uri();?>/dist/images/fifth.svg" class="fifth_elem" alt="">
            <span>4</span>
        </div>
    </section>

    <section class="page_not_found">
        <div class="missing_details">
            <h3 class="simple-title white-title">СТОРІНКА НЕ ЗНАЙДЕНА</h3>
            <p>Сторінка,  на яку ви намагаєтесь перейти не знайдена.</p>
            <p> Можливо, Ви ввели не правильну адресу, або сторінка була видалена.   </p>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn product-btn">Головна сторінка</a>
        </div>
    </section>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        var window_height =  jQuery(window).height();
        var header_height =  jQuery('header').height();
        var footer_height =  jQuery('footer').height();
        var page_height = window_height - header_height - footer_height;
        jQuery('.missing_wrap').css('height', page_height-2);
        jQuery('.not_found').css('height', page_height * 0.47);
        jQuery('.page_not_found').css('height', page_height - page_height * 0.47);

        jQuery('.close-menu').click(function() {
            jQuery('#menu').css('display', 'none');
        })
    });
</script>
