<?php while (have_posts()) : the_post(); ?>

	<ul  class="breadcrumbs">
		<?php yoast_breadcrumb('<li>','</li>');?>
	</ul>

	<section class="single-product">
		<div class="row page-container">
			<div class="item-slider">
				<div class="slider-for">
					<div class="pr-item">
						<img src="<?php the_field('thumbnail');?>" alt="image" draggable="false">
					</div>
					<?php  $images = get_field('gallery'); ?>
					<?php if( $images ): ?>
						<?php foreach( $images as $image ): ?>
							<div class="pr-item">
								<img src="<?php echo $image['url']; ?>" alt="image" draggable="false">
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="slider-nav">

					<div class="pr-item">
						<img src="<?php the_field('thumbnail');?>" alt="image" draggable="false">
					</div>

					<?php if( $images ): ?>
						<?php foreach( $images as $image ): ?>
							<div class="pr-item">
								<img src="<?php echo $image['url']; ?>" alt="image" draggable="false">
							</div>
						<?php endforeach; ?>
					<?php endif; ?>

				</div>
			</div>
			<div class="product-detail">
				<h4 class="cat-title"><?php the_title(); ?></h4>
				<div class="item-price"><?php the_field('price_string');?></div>
				<p>Розмір:  <?php the_field('dimentions');?> </p>
				<p><?php the_field('description');?></p>

				<?php if( have_rows('colours') ):?>


					<div class="colors">
						<p>Доступні кольори</p>
						<div>

							<?php while ( have_rows('colours') ) : ?>
								<?php the_row(); ?>
								<span class="color-filter white" style="background-color: <?php the_sub_field('colour');?>;"></span>
							<?php  endwhile; ?>
						</div>
					</div>

				<?php endif; ?>


				<button class="btn product-btn order-item" data-open="order_item">Замовити</button>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>


	<section class="our_products catalogue recommend">
		<div class="row">
			<h4 class="prod-name columns">Ми рекомендуємо</h4>


			<?php $posts = get_field('related'); ?>

			<?php if( $posts ): ?>

				<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>

					<?php get_template_part( 'templates/block', 'loop_one_product' );?>

					<?php setup_postdata($post); ?>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>

		</div>
	</section>
<?php endwhile; ?>


<script type="text/javascript">
	jQuery(document).ready(function() {
		var contact_height =  jQuery('.contacts').height();
		jQuery('.contact').css('height', contact_height);

		var container =  jQuery('.about_blocks .page-container').width();
		var img_width =  jQuery('.about_blocks img').width();
		var img_height =  jQuery('.about_blocks img').height();
		jQuery('.block-text').css({'width': container - img_width, 'height': img_height});

		jQuery('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-nav',
			autoplay: true,
			autoplaySpeed: 5000,
			verticalSwiping: true,
		});
		jQuery('.slider-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			dots: false,
			arrows: false,
			centerMode: true,
			focusOnSelect: true,
			vertical: true,
		});

		var slider = jQuery('.slider-nav').slick( "getSlick" );
		var count_items = slider.slideCount;

		if(count_items == 1) {
			jQuery('.slider-nav .slick-list').addClass('one-item');
		} else if (count_items == 2) {
			jQuery('.slider-nav .slick-list').addClass('two-item');
		} else if (count_items == 3) {
			jQuery('.slider-nav .slick-list').addClass('three-item');
		}

		jQuery('.slider-for .pr-item img').each(function() {
			var sl_width = jQuery(this).width();
			var sl_height = jQuery(this).height();
			if (sl_width/sl_height > 1) {
				jQuery(this).css('width', '100%');
			}
		});

		jQuery('.close-menu').click(function() {
			jQuery('#menu').css('display', 'none');
		});
		if (jQuery(window).width() < 640) {
			jQuery("#order_item .other-cont").insertAfter(".ordered-item");
		}

	});
</script>